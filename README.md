# imaging-mask-demonstration

## Description

This is an image dataset for the course **Experimental Techniques in Ultrafast Science** taught during winter semester 2023 at University of Göttingen.

## Instruction

The dataset contains an SEM image of the sample, an IPython notebook for data analysis, and an npz file containing the data acquired with EUV imaging at approximately 54 eV.

## Authors and acknowledgment
The dataset is taken by Hung-Tzu Chang and Sergey Zayko in the Department of Ultrafast Dynamics (AG Ropers) at Max Planck Institute for Multidisciplinary Sciences.

## License
This dataset is distributed under [CC-BY-4.0](https://creativecommons.org/licenses/by/4.0/) license.
